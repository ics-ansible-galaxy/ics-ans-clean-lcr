ics-ans-clean-lcr
=================

Ansible playbook to run cleaning tasks on the LCR workstations.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
